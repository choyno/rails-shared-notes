# Change or Setup your App using PostgreSQL

### Update your GEMFILE and add the pg gem

```
    gem 'pg'
```

### bundle installl to isntall the new added gem

```
   bundle install
```

### update your database.yml to connect using the postgresql server

```

default: &default
  adapter: postgresql
  encoding: unicode
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  username: < db user >
  timeout: 5000

development:
  <<: *default
  database: <dev db name >

test:
  <<: *default
  database: <test db name >

production:
  <<: *default
  database: <prod db name >
  username: postgres
  password:


```


### How to create PostgreSql db using console Terminal

check version
```
   psql --version  
```

connect to the psql server in your local using the terminal
```
    connect = psql postgres  
```

```
    CREATE USER <username>;  
    CREATE DATABASE <db_name_development> OWNER <username>;  
    GRANT ALL PRIVILEGES ON DATABASE <db_name_development> to <username>;  

```

Adding Permission  
```
    alter role <username> with superuser;  
    ALTER DATABASE <db_name_development> OWNER TO <username>;  
    quit =\q  
```



