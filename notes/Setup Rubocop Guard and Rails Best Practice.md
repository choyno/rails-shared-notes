# SETUP

**Create Rubocup / Guard / Rails Best Practice Setup**

### Installing Rubocop

Install Rubocop by adding it to your Gemfile:

```
    gem 'rubocop', require: false
    gem 'rubocop-rails', require: false
    gem 'rubocop-performance', require: false
```

And run the bundle installer:

```
    bundle install
```

### Configuring Rubocop

Place the following `.rubocop.yml` file in your project root or in your WSL home folder, overwriting any existing file if one was already present. (Note: Your WSL home folder is where you navigate if you run: `cd ~`)

The config file:

```
AllCops:
  TargetRubyVersion: <RUBY VERSION TO CHECK>
  NewCops: enable

  Exclude:
    - db/migrate/**/*
    - db/schema.rb
    - config/**/*
    - script/**/*
    - bin/**/*
    - test/**/*
    - app/admin/**/*
    - app/channels/**/*
    - app/jobs/**/*
    - node_modules/**/*
    - Gemfile
    - Rakefile
    - config.ru
    - 'config/settings/**/*'

require:
  - rubocop-performance
  - rubocop-rails

Style/Encoding:
  Enabled: false

Style/Documentation:
  Description: 'Document classes and non-namespace modules.'
  Enabled: false

Style/InlineComment:
  Description: 'Avoid inline comments.'
  Enabled: false

Layout/LineLength:
  Description: 'Limit lines to 100 characters. (Default is 80)'
  Max: 100

Style/FrozenStringLiteralComment:
  Description: To help transition from Ruby 2.3.0 to Ruby 3.0.
  Enabled: false

Style/WordArray:
  Description: 'Use %w or %W for arrays of words.'
  Enabled: false

# Defaults all strings to double quotes. Less performant, but
# nicer for consistency, and for adding interpolation later.
Style/StringLiterals:
  EnforcedStyle: double_quotes

# Prettier hashes.
Layout/HashAlignment:
  EnforcedHashRocketStyle: table
  EnforcedColonStyle: table

# No auto-correct for unused block arguments,
# but will still warn.
Lint/UnusedBlockArgument:
  AutoCorrect: false

# No auto-correct for unused method arguments,
# but will still warn.
Lint/UnusedMethodArgument:
  AutoCorrect: false

Rails/HasAndBelongsToMany:
  Enabled: false

```

### Running Rubocop

Wih the config file in place run the following command from your project root:

```
    bundle exec rubocop
```

You can also auto-correct all sorts of errors with:

    bundle exec rubocop --safe-auto-correct

If you are seeing lots of error please check to see if you properly saved the rubocop config file to you project root directory or to your home folder (not your Windows home, but the WSL one: `~/.rubocop.yml`).

### Further Rubocop Configuration

Feel free to add other folders and files to the `Exclude` section if they don't contain code that you personally wrote. 

If you are getting rubocop errors due to [ABC Size](https://www.rubydoc.info/gems/rubocop/0.27.0/RuboCop/Cop/Metrics/AbcSize) or [Cyclomatic Complexity](https://www.rubydoc.info/gems/rubocop/RuboCop/Cop/Metrics/CyclomaticComplexity) you can disable these. Just be sure to note that these are indicators that your methods are growing overly complex. :) 

Disabling others 'cops' is also allowed if you obtain approval from your instructor.

### Enable Cache permission

```
sudo chown -R $USER $HOME/.cache/
```


### AUTO RELOAD USING  GUARD FILE

GUARD - https://github.com/guard/guard
GUARD RUBOCOP - https://github.com/rubocop/guard-rubocop

add the guard and guard-rubocop
```
	 gem 'guard', require: false
	 gem 'guard-rubocop', require: false
```

add additional setup to your `.rubocop.myl`
```
AllCops
	Include:
		 - '**/*.rb'
		 - '**/*.rake'
		 - '**/config.ru'
		 - '**/Gemfile'
		 - '**/Guardfile'
		 - '**/Rakefile'
```

Paste under  `GUARDFILE`
https://github.com/guard/guard


enable guard to watch over the rubocop all the time
```
guard :rubocop, all_on_start: true, cli: ['--auto-correct', '--display-style-guide', '--display-cop-names'] do
		watch(/.+\.rb$/)
		watch(/.+\.rake$/)
		watch(/^\.rubocop\.yml$/)
end
~
```


### Rails Best Practice
https://github.com/flyerhzm/rails_best_practices
https://www.rubydoc.info/gems/rails_best_practices-gorgeouscode


Add Gem FIle
```
 gem 'guard-rails_best_practices', git: "https://github.com/logankoester/guard-rails_best_practices.git"
 gem 'rails_best_practices', require: false
```

run the bundle installer:

```
    bundle install
```

add this additional setup in your GUARDFILE
```
guard :rails_best_practices do
	watch(%r{^app/(.+).rb$})
	watch(/.+\.rb$/)
end
```

### Run Guard Command
this should watch both rubocop linter and the Rails Best Practice Gem
```
  bundle exec guard
```
