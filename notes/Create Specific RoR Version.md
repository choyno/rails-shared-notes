
# Create Specific RoR Version #

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### Make directory for new rails app

```
mkdir app
cd app
```

### Specify ruby version


[https://rubygems.org/gems/rails/versions](https://rubygems.org/gems/rails/versions)
https://guides.rubyonrails.org/command_line.html

RVM:  https://cheatsheetmaker.com/rvm-cheat-sheet-cheat-sheet
```
echo 2.7.0 > .ruby-version

echo "source 'https://rubygems.org'" > Gemfile
OR echo "gem 'rails', '~> 6.0.3'" >> Gemfile
echo "gem 'rails', '~> 6.1.3'" >> Gemfile

```

### Create and Assign to Ruby version and Gemset

```
rvm gemset use 2.7.0@appname --create

bundle install
```

### create rails app using specified version, and overwrite current Gemfile

```
bundle exec rails new . --force --skip-bundle
bundle exec rails new . --force --skip-bundle --skip-test-unit --database=postgresql
bundle exec rails new . --force --skip-bundle --skip-test-unit --api
```

### Re intall bundler

```
gem install bundler

or

sudo gem install bundler
```

---

### Setting update using Webpack ex.  React On Rails

```
bundle exec rails new . --force --webpact=react --database=postgresql
```
```
rails new rails-react-todo-list --webpact=react --database=postgresql
```

### Installing React on Existing Project

```
 bundle exec rails webpacker:install:react
 ```
